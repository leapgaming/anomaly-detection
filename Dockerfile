FROM phpdockerio/php72-fpm:latest
WORKDIR "/application"

# Fix debconf warnings upon build
ARG DEBIAN_FRONTEND=noninteractive


# Install selected extensions and other stuff
RUN apt-get update \
    && apt-get install -y --no-install-recommends gnupg \
    && echo "deb http://ppa.launchpad.net/ondrej/php/ubuntu bionic main" > /etc/apt/sources.list.d/ondrej-php.list \
    && apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 4F4EA0AAE5267A6C \
    && apt-get update \
    && apt-get -y --no-install-recommends install nginx vim git php7.2-mysql php7.2-gd php-xdebug php7.2-bcmath php7.2-bz2 php7.2-gd php7.2-intl php7.2-ssh2 php7.2-xsl php-yaml cron libopenblas-dev liblapacke-dev php-phpseclib gfortran re2c php-seclib php7.2-dev php-pear \
    && apt-get clean; rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*



# Install phalcon
RUN curl -s https://packagecloud.io/install/repositories/phalcon/stable/script.deb.sh | bash \
    && apt-get install -y php7.2-phalcon=3.4.2-2+php7.2 \
    && apt-get clean; rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*

# Phalcon dev tools
RUN cd ~  && git clone git://github.com/phalcon/phalcon-devtools.git --branch 3.4.x \
    && export PTOOLSPATH=/root/phalcon-devtools \
    && cd phalcon-devtools/ && ./phalcon.sh \
    && ln -s ~/phalcon-devtools/phalcon /usr/bin/phalcon  \
    && chmod ugo+x /usr/bin/phalcon

# Installing php stats
RUN apt-get update \
    && pecl install -a stats-2.0.3 \
    && pecl install tensor-2.2.3 \
    && echo "extension=stats.so" >> /etc/php/7.2/fpm/php.ini \
    && echo "extension=stats.so" >> /etc/php/7.2/cli/php.ini \
    && echo "extension=tensor.so" >> /etc/php/7.2/fpm/php.ini \
    && echo "extension=tensor.so" >> /etc/php/7.2/cli/php.ini

RUN mkdir /var/www/app \
  && rm /etc/nginx/sites-enabled/default

COPY ./app /var/www/app
COPY /config/nginx/nginx.conf /etc/nginx/sites-enabled/default.conf

CMD ["sh", "-c", "/usr/sbin/cron; /usr/sbin/nginx; /usr/bin/php-fpm;"]
