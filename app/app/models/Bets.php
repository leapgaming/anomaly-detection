<?php

namespace Models;

use ClickHouseDB\Client;
use ClickHouseDB\Type\UInt64;

class Bets
{
    /**
     * @return Client for ClickHouse DB
     */
    public static function getDb()
    {
        /** @var Client $db */
        return \Phalcon\Di::getDefault()->get("ClickHouseDB");
    }

    public static function addBets($bet)
    {
        $fields = [
            "stake_eur",
            "return_eur",
            "player_id",
            "game_id",
            "game_type_id"
        ];
        $data = [];

        $data[] = [
            isset($bet['stake_eur']) ? floatval($bet['stake_eur']) : 0,
            isset($bet['return_eur']) ? floatval($bet['return_eur']) : 0,
            isset($bet['player_id']) ? UInt64::fromString($bet['player_id']) : 0,
            isset($bet['game_id']) ? UInt64::fromString($bet['game_id']) : 0,
            isset($bet['game_type_id']) ? UInt64::fromString($bet['game_type_id']) : 0,
        ];

        static::getDb()->insert(
            'bets',
            $data,
            $fields
        );
    }

    public static function getPlayerData($playerId, $gameId)
    {
        $dataSet = static::getDb()->select(
            "SELECT game_id, count(*) AS count_bets,
                   round(coalesce(sum(return_eur)/sum(stake_eur),0) * 100, 4)  AS rtp,
                   round((sum(if(return_eur > 0, 1, 0)) /count(*)) * 100, 4) won
              FROM bets
             WHERE player_id = :player
               AND game_id = :game
             GROUP BY player_id, game_id
             ORDER BY player_id ",
            [
                'player' => $playerId,
                'game' => $gameId
            ]
        )->rows();

        return $dataSet;
    }

    /**
     * Bind parameters for PDO templates
     *
     * @param string $prefix
     * @param array $values
     * @param array $bindArray
     * @return string
     */
    public static function bindParamArray(string $prefix, array $values, array &$bindArray)
    {
        $str = "";
        foreach ($values as $item) {
            $str .= ":" . $prefix . $item . ",";
            $bindArray[$prefix . $item] = $item;
        }
        return rtrim($str, ",");
    }
}