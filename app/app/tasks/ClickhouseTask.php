<?php

namespace Tasks;

use Phalcon\Cli\Task;
use Library\ConsoleTask\Helper;

/**
 * Class ClickhouseTask
 *
 * Example command to run:
 *
 * @command docker-compose exec php-fpm bash
 * run task:
 * @command php /application/app/app/cli.php clickhouse migrate
 * @package Tasks
 */
class ClickhouseTask extends Task
{
    /**
     * Store mext migration number, if it exist
     *
     * @var int $nextMigrationNumber
     */
    protected $nextMigrationNumber;

    /**
     * Store migrations file name
     *
     * @var array $migrations
     */
    protected $migrations;

    /**
     * Store path to migrations history file
     *
     * @var string $migrationVersionFile
     */
    protected $migrationVersionFile = '.phalcon/click-house-migration-version';

    /**
     * Store path to dir with migrations
     *
     * @var string $migrationsDir
     */
    protected $migrationsDir = 'migrations';

    /**
     * Store queries for migrations
     *
     * @var array $queries
     */
    protected $queries;

    /**
     * Store name of processed file from migration
     *
     * @var string $currentFile
     */
    protected $currentFile = '';

    /**
     * Name of db connection
     *
     * @var string
     */
    protected $dbName = "ClickHouseDB";

    /**
     * Array with variables
     *
     * @var array $variables
     */
    protected $variables = [];

    /**
     * Default action
     */
    public function mainAction()
    {
        echo "Helper: ". PHP_EOL;
        echo "migrate - apply migrations from files in 'clickhouse_migrations' dir;". PHP_EOL;
    }

    /**
     * Run workflow to migrate new tables to ClickHouse
     *
     * * run task:
     * @command php /application/app/bootstrap_cli.php clickhouse migrate
     */
    public function migrateAction($params)
    {
        if (!empty($params[0])) {
            $this->dbName = $params[0];
        }
        $this->migrationVersionFile = ".phalcon/click-house-migration-version-" . $this->dbName;
        $this->getVariables();
        try {
            if (file_exists(APP_PATH . "/" . $this->migrationVersionFile)) {
                $this->checkMigrationVersion();
            } else {
                $handle = fopen(APP_PATH . "/" . $this->migrationVersionFile, "w+");
                fclose($handle);
                chmod(APP_PATH . "/" . $this->migrationVersionFile, 0777);
                $this->nextMigrationNumber = 1;
            }

            $this->getMigrationFileNames();
            if (!empty($this->migrations)) {
                foreach ($this->migrations as $file) {
                    $this->currentFile = $file;
                    $this->readMigrationFile($file);
                    $this->executeQueries();
                    file_put_contents(
                        APP_PATH . "/" . $this->migrationVersionFile,
                        PHP_EOL . stristr($file, '.', true),
                        FILE_APPEND
                    );
                }
            } else {
                echo "All migrations applied." . PHP_EOL;
            }

        } catch (\Exception $exception) {
            echo "ERROR: $this->currentFile" . PHP_EOL;
            echo $exception->getMessage();
            die;
        }
    }

    /**
     * Get variables for migrations template
     */
    public function getVariables()
    {
        $config = $this->getDI()->getShared('config')->toArray();
        $this->variables['database'] = $config[$this->dbName]['database'] ?? 'leap';
        $this->variables['serverName'] = $config[$this->dbName]['name'] ?? 'ch1';
    }

    /**
     * Replace variables in template
     *
     * @param $template
     * @return array|mixed|string|string[]
     */
    public function replaceVariables($template)
    {
        if (preg_match_all("/{{(.*?)}}/", $template, $matches)) {
            foreach (array_unique($matches[1]) as $idx => $variableName) {
                $template = str_replace('{{' . $variableName . '}}', $this->variables[$variableName], $template);
            }
        }
        return $template;
    }

    /**
     * Run workflow to migrate new tables to ClickHouse
     *
     * * run task:
     * @command php /application/app/bootstrap_cli.php clickhouse createDb
     */
    public function createDbAction($params)
    {
        $dbName = (!empty($params[0])) ? trim(strval($params[0])) : null;

        if (empty($dbName)) {
            Helper::log("Error! Please enter DB name.");
            return;
        }

        try {
            $this->migrationsDir = 'migrations/clickhouse_schema';
            $this->getMigrationFileNames();
            if (!empty($this->migrations)) {
                foreach ($this->migrations as $file) {
                    $this->currentFile = $file;
                    $this->readMigrationFile($file);
                    $this->setDbNameToQueries($dbName);
                    $this->executeQueries();
                }
            } else {
                echo "All migrations applied." . PHP_EOL;
            }

        } catch (\Exception $exception) {
            echo "ERROR: $this->currentFile" . PHP_EOL;
            echo $exception->getMessage();
            die;
        }
    }


    /**
     * Check if exist not applied migrations
     */
    public function checkMigrationVersion()
    {
        $data = file(APP_PATH . "/" . $this->migrationVersionFile);
        if (!empty($data)) {
            $line = end($data);
            $this->nextMigrationNumber = intval($line) + 1;
        } else {
            $this->nextMigrationNumber = 1;
        }
    }

    /**
     * Get not applied migrations file name
     */
    public function getMigrationFileNames()
    {
        $dirEntity = scandir(APP_PATH . "/" . $this->migrationsDir);
        sort($dirEntity, SORT_NUMERIC);
        $i = 0;
        foreach ($dirEntity as $item) {
            if (stristr($item, '.', true) >= $this->nextMigrationNumber) {
                $this->migrations[$i] = $item;
                $i++;
            }
        }
    }

    /**
     * read Migration File and create queries
     *
     * @param $file
     */
    public function readMigrationFile($file)
    {
        $file = file(APP_PATH . "/" . $this->migrationsDir . '/' . $file);
        $queries = [];
        $i = 0;
        foreach ($file as $row) {
            if ($row == "\n" || $row == " \n" || $row == "\n ") {
                $i++;
            } else {
                $queries[$i][] = $row;
            }
        }

        foreach ($queries as $query) {
            $this->queries[] = implode(' ', $query);
        }
    }

    /**
     * Execute queries
     *
     * @throws \Exception
     */
    public function executeQueries()
    {
        if (empty($this->queries)) {
            return;
        }

        $i = 1;
        foreach ($this->queries as $query) {
            if (!empty($query)) {
                $query = $this->replaceVariables($query);
                $this->curlQuery($query, $i);
                $i++;
                echo PHP_EOL;
            }
        }
        $this->queries = null;
    }

    /**
     * Set db name to queries
     *
     */
    public function setDbNameToQueries($dbName)
    {
        if (empty($dbName) || empty($this->queries)) {
            return;
        }

        array_walk($this->queries, function (&$query) use ($dbName) {
            $query = str_replace('::DBNAME::', $dbName, $query);
        });
    }

    /**
     * Send queries to ClickHouse server
     *
     * @param $query
     * @param $num
     * @throws \Exception
     */
    public function curlQuery($query, $num)
    {
        $config = $this->getDI()->getShared("config")->toArray();
        $url = $config[$this->dbName]['host'] . ":" . $config[$this->dbName]['port']
            . "/?user=" . $config[$this->dbName]['username'];

        if ($config[$this->dbName]['password']) {
            $url .= "&password=" . $config[$this->dbName]['password'];
        }

        $options = [
            CURLOPT_POST => true,
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_HEADER => false,
            CURLOPT_URL => $url,
            CURLOPT_POSTFIELDS => $query
        ];
        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $error = curl_error($ch);
        $result = curl_exec($ch);
        $requestInfo = curl_getinfo($ch);
        curl_close($ch);

        if (!empty($error)) {
            throw new \Exception($error);
        } else {
            if (stristr($result, 'Exception')) {
                throw new \Exception($result);
            } else {
                if ($result !== false) {
                    echo $this->currentFile . " query# {$num} - Successfully applied" . PHP_EOL;
                } else {
                    echo $result;
                    $error = "cURL error: HTTP code: {$requestInfo['http_code']}, Time: {$requestInfo['total_time']},";
                    $error .= " Download content length: {$requestInfo['download_content_length']}" . PHP_EOL;
                    throw new \Exception($error);
                }
            }
        }
    }

    /**
     * Fix -nan value in columns important for statistic
     *
     * * run task:
     * @command php /application/app/bootstrap_cli.php clickhouse fixnan
     */
    public function fixNanAction()
    {
        $counter = 0;
        $startTime = microtime(true);

        try {
            $connection = $this->getDI()->getShared("ClickHouseDB2");
            $this->fixPnlNan($connection, 'bets', $counter);
            $this->fixStakeNan($connection, 'bets', $counter);
            $this->fixReturnNan($connection, 'bets', $counter);
        } catch (\Exception $exception) {
            Helper::log("Finished with error:");
            echo $exception->getMessage();
            echo "Recorded {$counter} rows.";
        }

        Helper::log("Execution time: " . (microtime(true) - $startTime) . 'sec' . "updated {$counter} row(s)");
    }

    /**
     * Fix -nan in pnl column
     *
     * @param $connection
     * @param $table
     * @param $counter
     */
    public function fixPnlNan($connection, $table, &$counter)
    {
        $nanPnl = $connection->select(
            "SELECT bet_id_unique, stake_eur, return_eur
                   FROM {$table}
                   WHERE isNaN(pnl_eur) OR isInfinite(pnl_eur)",
            []
        )->rows();

        if (!empty($nanPnl)) {
            foreach ($nanPnl as $bet) {
                $pnl = $bet['stake_eur'] - $bet['return_eur'];
                $result = $connection->write(
                    "ALTER TABLE {$table} UPDATE pnl_eur={$pnl} where bet_id_unique = {$bet['bet_id_uniq']}"
                );
                $counter++;
            }
        }
    }

    /**
     * Fix -nan in stake_eur column
     *
     * @param $connection
     * @param $table
     * @param $counter
     */
    public function fixStakeNan($connection, $table, &$counter)
    {
        $nanPnl = $connection->select(
            "SELECT bet_id_unique, pnl_eur, return_eur
                   FROM {$table}
                   WHERE isNaN(stake_eur) OR isInfinite(stake_eur)",
            []
        )->rows();

        if (!empty($nanPnl)) {
            foreach ($nanPnl as $bet) {
                $stake = $bet['pnl_eur'] + $bet['return_eur'];
                $result = $connection->write(
                    "ALTER TABLE leap.{$table} UPDATE stake_eur={$stake} where bet_id_unique = {$bet['bet_id_uniq']}"
                );
                $counter++;
            }
        }
    }

    /**
     *  fix pnl in Return column
     *
     * @param $connection
     * @param $table
     * @param $counter
     */
    public function fixReturnNan($connection, $table, &$counter)
    {
        $nanPnl = $connection->select(
            "SELECT bet_id_unique, stake_eur, pnl_eur
                   FROM {$table}
                   WHERE isNaN(return_eur) OR isInfinite(return_eur)",
            []
        )->rows();

        if (!empty($nanPnl)) {
            foreach ($nanPnl as $bet) {
                $return = $bet['stake_eur'] - $bet['pnl_eur'];
                $result = $connection->write(
                    "ALTER TABLE leap.{$table} UPDATE return_eur={$return} where bet_id_unique = {$bet['bet_id_uniq']}"
                );
                $counter++;
            }
        }
    }
}
