<?php

namespace Tasks;

use Library\AnomalyDetection\ModelsFactory;
use Rubix\ML\Datasets\Unlabeled;
use Rubix\ML\Extractors\CSV;
use Rubix\ML\Transformers\NumericStringConverter;

class TrainTask extends TaskBase
{
    public function aiAction($params)
    {
        $this->parseParams($params);
        $startTime = microtime(true);
        ini_set('memory_limit', '-1');
        $dataSetLocation = static::FILE_PATH . $this->getFilename();
        static::log($dataSetLocation);
        $dataset = Unlabeled::fromIterator(new CSV($dataSetLocation, ","))->apply(new NumericStringConverter());
        $factory = new ModelsFactory($this->getDetector(), static::DEFAULT_DATA_TYPE, $this->getPrefix());
        $trained = $factory->create($dataset);
        $response = [
            "status" => 1,
            "trained" => $trained,
            "detector" => $this->getDetector(),
            "dataSet" => static::DEFAULT_DATA_TYPE,
            "sourceFile" => $this->getFilename(),
            "modelName" => ModelsFactory::modelsNameManager(
                static::DEFAULT_DATA_TYPE,
                $this->getDetector(),
                $this->getPrefix()
            ),
            "time" => round(microtime(true) - $startTime, 3)
        ];
        static::log($response);
    }
}