<?php

namespace Tasks;

use Phalcon\Cli\Task;

class TaskBase extends Task
{
    const DEFAULT_DETECTOR = "GaussianMLE";
    const DEFAULT_DATA_TYPE = "game-id-bets-rtp-win-by-player";
    const FILE_PATH = APP_PATH . '/tmp/';
    const DEFAULT_FILE_NAME = 'all-db-19-april.csv';

    public $detector;
    public $filename;
    public $params;
    public $prefix;

    public static function log($value, $time = null)
    {
        $string = is_string($value) || is_numeric($value) ? $value : json_encode($value, JSON_PRETTY_PRINT);
        if ($time) {
            echo date('c', time()), "\t", $string, PHP_EOL;
        } else {
            echo $string, PHP_EOL;
        }
    }

    public function parseParams($params)
    {
        if (!empty($params[0])) {
            $this->prefix = $params[0];
        } else {
            $this->prefix = false;
        }

        if (!empty($params[1])) {
            $this->detector = $params[1];
        } else {
            $this->detector = static::DEFAULT_DETECTOR;
        }
        if (!empty($params[2])) {
            $this->filename = $params[2];
        } else {
            $this->filename = static::DEFAULT_FILE_NAME;
        }

        $this->params = $params;
    }

    /**
     * @return mixed
     */
    public function getDetector()
    {
        return $this->detector;
    }

    /**
     * @return mixed
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @return bool|string
     */
    public function getPrefix()
    {
        return $this->prefix;
    }
}