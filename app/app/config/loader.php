<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerDirs(
    [
        $config->application->controllersDir,
        $config->application->modelsDir,
        $config->application->libraryDir,
        $config->application->tasksDir,
    ]
);

/**
 * Register Namespaces
 */
$loader->registerNamespaces([
    'Controllers' => APP_PATH . '/controllers/',
    'Models' => APP_PATH . '/models/',
    'Library' => APP_PATH . '/library/',
    'Tasks' => APP_PATH . '/tasks/',
]);

$loader->register();
