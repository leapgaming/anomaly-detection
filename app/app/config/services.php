<?php

use ClickHouseDB\Client;

/**
 * Shared configuration service
 */
$di->setShared('config', function () {
    return include APP_PATH . "/config/config.php";
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('ClickHouseDB', function () {
    $config = $this->getConfig();
    $connect = [
        'host' => $config->ClickHouseDB->host,
        'port' => $config->ClickHouseDB->port,
        'username' => $config->ClickHouseDB->username,
        'password' => $config->ClickHouseDB->password,
    ];

    $connection = new Client($connect);

    $connection->database($config->ClickHouseDB->database);

    return $connection;
});


