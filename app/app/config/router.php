<?php

$router = $di->getRouter();

$router->add(
    "/:controller/:action/:params",
    [
        "namespace" => "Controllers",
        "controller" => 1,
        "action" => 2,
        "params" => 3,
    ]
);

// Define your routes here

$router->handle();
