<?php

use Library\AnomalyDetection\ModelsFactory;
use Rubix\ML\Datasets\Unlabeled;
use Rubix\ML\Extractors\CSV;
use Rubix\ML\Transformers\NumericStringConverter;
use Library\AnomalyDetection\DataExtractor;
use Library\AnomalyDetection\Core;

class CheckBetsController extends ControllerBase
{
    public function indexAction()
    {
        $startTime = microtime(true);
        $stepTime = $startTime;
        $preparedData = [];
        $response = [
            "status" => 0
        ];

        $diffFields = array_diff(['stake_eur', 'return_eur', 'player_id', 'game_id'], array_keys($this->params));
        if (count($diffFields) == 0) {
            \Models\Bets::addBets($this->params);
            $this->profiler($response, $stepTime, 'insert');
            $playersData = \Models\Bets::getPlayerData($this->params['player_id'], $this->params['game_id']);
            $this->profiler($response, $stepTime, 'select');
            $extractor = new DataExtractor(static::DEFAULT_DATA_TYPE);
            $samples = $extractor->prepareUnlabeledData($playersData);
            $scores = ModelsFactory::checkModel(
                $this->getDetector(),
                static::DEFAULT_DATA_TYPE,
                $samples,
                $this->getPrefix()
            );
            $this->profiler($response, $stepTime, 'score');
            if (isset($this->params['learn']) && intval($this->params['learn']) === 1) {
                $factory = new ModelsFactory($this->getDetector(), static::DEFAULT_DATA_TYPE, $this->getPrefix());
                $trained = $factory->partial(new Unlabeled($samples));
                $this->profiler($response, $stepTime, 'learn');
                $response['trained'] = $trained;
            }
            foreach ($scores as $idx => $score) {
                $samples[$idx][] = round($score, 3);
                foreach ($samples[$idx] as $index => $sample) {
                    $preparedData[$this->params['player_id']][Core::DATA_TYPES[static::DEFAULT_DATA_TYPE]['titles'][$index]] = $sample;
                }
            }
            $response['status'] = 1;
            $response['scores'] = $preparedData;
            $response['time']['total'] = round(microtime(true) - $startTime, 6);
            $response['modelName'] = ModelsFactory::modelsNameManager(
                static::DEFAULT_DATA_TYPE,
                $this->getDetector(),
                $this->getPrefix()
            );
            $response['detector'] = $this->getDetector();
        } else {
            $response['error'] = "Fields" . implode(',', $diffFields) . " is required";
        }
        return $this->json($response);
    }

    public function profiler(&$response, &$time, $point)
    {
        $response['time'][$point] = round(microtime(true) - $time, 6);
        $time = microtime(true);
    }
}