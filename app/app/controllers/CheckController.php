<?php


use Library\AnomalyDetection\ModelsFactory;
use Library\AnomalyDetection\DataExtractor;

class CheckController extends ControllerBase
{
    public function indexAction()
    {
        $response  = [
            "status" => 0
        ];
        if (isset($this->params['samples'])) {
            $samples = $this->params['samples'];
            $extractor = new DataExtractor(static::DEFAULT_DATA_TYPE);
            $preparedSamples = $extractor->prepareUnlabeledData($samples);
            $scores = ModelsFactory::checkModel(
                $this->detector,
                static::DEFAULT_DATA_TYPE,
                $preparedSamples,
                $this->getPrefix()
            );
            $counter = 0;
            foreach ($samples as $idx => $item) {
                $samples[$idx]['score'] = round($scores[$counter], 3);
                $counter++;
            }

            $response['status'] = 1;
            $response['samples'] = $samples;
            $response['modelName'] = ModelsFactory::modelsNameManager(
                static::DEFAULT_DATA_TYPE,
                $this->getDetector(),
                $this->getPrefix()
            );
            $response['detector'] = $this->getDetector();

        } else {
            $response['error'] = "Samples is required";
        }
        return $this->json($response);
    }
}