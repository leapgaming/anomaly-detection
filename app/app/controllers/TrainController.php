<?php

use Library\AnomalyDetection\ModelsFactory;
use Rubix\ML\Datasets\Unlabeled;
use Rubix\ML\Extractors\CSV;
use Rubix\ML\Transformers\NumericStringConverter;

class TrainController extends ControllerBase
{
    public function indexAction()
    {
        $startTime = microtime(true);
        ini_set('memory_limit', '-1');
        $dataSetLocation = static::FILE_PATH . $this->getFilename();
        $dataset = Unlabeled::fromIterator(new CSV($dataSetLocation, ","))
            ->apply(new NumericStringConverter());
        $factory = new ModelsFactory($this->getDetector(), static::DEFAULT_DATA_TYPE, $this->getPrefix());
        $response = [
            "status" => 1,
            "trained" => $factory->create($dataset),
            "detector" => $this->getDetector(),
            "dataSet" => static::DEFAULT_DATA_TYPE,
            "sourceFile" => $this->getFilename(),
            "modelName" => ModelsFactory::modelsNameManager(
                static::DEFAULT_DATA_TYPE,
                $this->getDetector(),
                $this->getPrefix()
            ),
            "time" => round(microtime(true) - $startTime, 3)
        ];
        return $this->json($response);
    }
}