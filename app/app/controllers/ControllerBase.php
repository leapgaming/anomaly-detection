<?php

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    const DEFAULT_DETECTOR = "GaussianMLE";
    const DEFAULT_DATA_TYPE = "game-id-bets-rtp-win-by-player";
    const FILE_PATH = APP_PATH . '/tmp/';
    const DEFAULT_FILE_NAME = 'all-db-19-april.csv';

    public $detector;
    public $filename;
    public $params;
    public $prefix;

    public function initialize()
    {
        $this->getParams();
    }

    /**
     * Get params from different sources
     *
     * @return array|bool|mixed|stdClass
     */
    public function getParams()
    {
        if ($this->request->getHeader('CONTENT_TYPE') == 'application/json') {
            $this->params = $this->request->getJsonRawBody(true);
        } else {
            if ($this->request->isPost()) {
                $this->params = $this->request->getPost();
            } else {
                $this->params = $this->request->get();
            }
        }
        $this->setDetector();
        $this->setFileName();
        $this->setPrefix();
        return $this->params;
    }

    /**
     * Set detector name
     */
    public function setDetector()
    {
        if (isset($this->params['detector'])) {
            $this->detector = $this->params['detector'];
            unset($this->params['detector']);
        } else {
            $this->detector = static::DEFAULT_DETECTOR;
        }

    }

    /**
     * Set detector name
     */
    public function setPrefix()
    {
        if (isset($this->params['prefix'])) {
            $this->prefix = $this->params['prefix'];
            unset($this->params['prefix']);
        } else {
            $this->prefix = false;
        }

    }

    /**
     *  Set file name
     */
    public function setFileName()
    {
        if (isset($this->params['file'])) {
            $this->filename = $this->params['file'];
            unset($this->params['file']);
        } else {
            $this->filename = static::DEFAULT_FILE_NAME;
        }
    }

    /**
     * @return mixed
     */
    public function getDetector()
    {
        return $this->detector;
    }

    /**
     * @return mixed
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @return bool|string
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * Json response
     *
     * @param array $data
     * @param int $ttl
     * @param string $type
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     * @throws \Exception
     */
    public function json($data = [], $ttl = 600, $type = "private")
    {
        $this->view->disable();
        $this->response->setHeader("Cache-Control", "$type, max-age=$ttl");
        $this->response->setContentType('application/json', 'UTF-8');
        $this->response->setJsonContent($data);

        return $this->response;
    }
}
