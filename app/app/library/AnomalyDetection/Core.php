<?php

namespace Library\AnomalyDetection;

class Core
{
    const SIZE_OF_DATA_PART = 100000;
    const BETS_AD_MODEL_NAME = "-AD-bets.model";
    const PATH_TO_MODEL_STORAGE = '/models/AnomalyDetectionModels/';
    const DETECTORS = [
        "GaussianMLE" => [
            "class" => "\Rubix\ML\AnomalyDetectors\GaussianMLE",
            "online" => true
        ],
//        "IsolationForest" => [
//            "class" => "\Rubix\ML\AnomalyDetectors\IsolationForest",
//            "online" => false
//        ],
        "Loda" => [
            "class" => "\Rubix\ML\AnomalyDetectors\Loda",
            "online" => true
        ],
//        "RobustZScore" => [
//            "class" => "\Rubix\ML\AnomalyDetectors\RobustZScore",
//            "online" => false
//        ]
    ];
    const DATA_TYPES = [
        'game-id-bets-rtp-win-by-player' => [
            'titles' => ['game id', 'bets', 'rtp,%', 'win,%', 'score'],

        ],
        'game-id-bets-rtp-win-by-player-by-hour' => [
            'titles' => ['game id', 'bets', 'rtp,%', 'win,%', 'score'],

        ],
        'game-id-bets-win-by-player-by-hour' => [
            'titles' => ['game id', 'bets', 'win,%', 'score'],

        ],
    ];

    const CHECK_DATA_PRESETS = [
        'game-id-bets-rtp-win-by-player' => [
            [2060, 420, 93.45, 63.57],
            [5001, 3047, 115.47, 61.43],
            [2036, 2, 117.5, 50.0],
            [3002, 5, 3, 34],
            [3002, 40, 100, 44],
            [3002, 15, 1, 32],
            [3002, 13213, 1, 30],
            [3002, 1, 13000, 37],
            [3002, 13213, 300, 40],
            [3002, 1000, 13000, 45],
            [3002, 22, 799, 33],
            [3002, 51, 30, 34],
            [3002, 8, 9, 50],
            [3003, 8, 9, 80],
            [3002, 80, 100000, 12],
            [3002, 800, 100000, 36],
            [3003, 8, 100000, 43],
            [3002, 94, 67, 33],
            [3002, 7542, 94, 22],
            [3003, 7542, 94, 22],
            [10013, 70, 94, 22],

        ],
        'game-id-bets-rtp-win-by-player-by-hour' => [
            [2060, 420, 93.45, 63.57],
            [5001, 3047, 115.47, 61.43],
            [2036, 2, 117.5, 50.0],
            [3002, 5, 3, 34],
            [3002, 40, 100, 44],
            [3002, 15, 1, 32],
            [3002, 13213, 1, 30],
            [3002, 1, 13000, 37],
            [3002, 13213, 300, 40],
            [3002, 1000, 13000, 45],
            [3002, 22, 799, 33],
            [3002, 51, 30, 34],
            [3002, 8, 9, 50],
            [3003, 8, 9, 80],
            [3002, 80, 100000, 12],
            [3002, 800, 100000, 36],
            [3003, 8, 100000, 43],
            [3002, 94, 67, 33],
            [3002, 7542, 94, 22],
            [3003, 7542, 94, 22],
            [10013, 70, 94, 22],

        ],
        'game-type-bets-rtp-win-by-player' => [
            [1, 420, 93.45, 63.57],
            [1, 3047, 115.47, 61.43],
            [1, 2, 117.5, 50.0],
            [4, 5, 3, 34],
            [4, 40, 100, 44],
            [4, 15, 1, 32],
            [4, 13213, 1, 30],
            [4, 1, 13000, 37],
            [4, 13213, 300, 40],
            [4, 1000, 13000, 45],
            [4, 22, 799, 33],
            [4, 51, 30, 34],
            [4, 8, 9, 50],
            [4, 8, 9, 80],
            [4, 80, 100000, 12],
            [4, 800, 100000, 36],
            [4, 8, 100000, 43],
            [4, 94, 67, 33],
            [4, 7542, 94, 22],
            [1, 7542, 94, 22],
            [1, 70, 94, 22],
        ]
    ];

    /**
     * Returns model name file
     *
     * @param string $dataTypeName
     * @param string $detectorName
     * @param string $prefix
     * @return string
     */
    public static function modelsNameManager($dataTypeName, $detectorName, $prefix = false)
    {
        if ($prefix) {
            return $prefix . '-' . hash('crc32', $dataTypeName) . "-Detector-{$detectorName}" .
                static::BETS_AD_MODEL_NAME;
        } else {
            return "default-" . hash('crc32', $dataTypeName) . "-Detector-{$detectorName}" .
                static::BETS_AD_MODEL_NAME;
        }
    }

    /**
     * Returns full path to model file
     *
     * @param string $dataTypeName
     * @param string $detectorName
     * @return string
     */
    public static function getModelLocation($dataTypeName, $detectorName, $prefix = false)
    {
        return APP_PATH . static::PATH_TO_MODEL_STORAGE .
            static::modelsNameManager($dataTypeName, $detectorName, $prefix);
    }
}
