<?php

namespace Library\AnomalyDetection;

use Library\ConsoleTask\Helper;
use Rubix\ML\Datasets\Dataset;
use Rubix\ML\Datasets\Unlabeled;
use Rubix\ML\Estimator;
use Rubix\ML\Persisters\Filesystem;
use Rubix\ML\Persisters\Persister;

class ModelsFactory extends Core
{
    public $detectorName;
    public $dataType;
    public $detectorConfig;
    public $dataConfig;
    public $prefix;

    /**
     * ModelsFactory constructor.
     *
     * @param $detectorName
     * @param $dataType
     */
    public function __construct($detectorName, $dataType, $prefix = false)
    {
        $this->detectorName = $detectorName;
        $this->dataType = $dataType;
        $this->prefix = $prefix;
        $this->detectorConfig = static::DETECTORS[$detectorName];
        $this->dataConfig = static::DATA_TYPES[$dataType];
    }

    /**
     * Create and learn detector model
     *
     * @return boolean
     */
    public function create($dataSet = null)
    {
        $limit = static::SIZE_OF_DATA_PART;
        $estimator = new $this->detectorConfig['class']();
        if ($dataSet) {
            $estimator->train($dataSet);
        } else {
            $extractor = new DataExtractor($this->dataType);
            if ($this->detectorConfig['online']) {
                while (true) {
                    $dataPart = $extractor->extractPart($limit);
                    $dataSet = new Unlabeled($dataPart);
                    $estimator->partial($dataSet);
                    if (count($dataPart) < $limit) {
                        break;
                    }
                }
            } else {
                $data = $extractor->extractAll();
                $dataSet = new Unlabeled($data);
                $estimator->train($dataSet);
            }
        }
        /** @var Persister $persister */
        $persister = new Filesystem(static::getModelLocation($this->dataType, $this->detectorName, $this->prefix));
        $persister->save($estimator);
        return $estimator->trained();
    }

    /**
     * @param $dataSet
     * @return mixed
     */
    public function partial($dataSet)
    {
        /** @var Persister $persister */
        $persister = new Filesystem(static::getModelLocation($this->dataType, $this->detectorName, $this->prefix));
        $estimator = $persister->load();
        $estimator->partial($dataSet);
        /** @var Persister $persister */
        $persister = new Filesystem(static::getModelLocation($this->dataType, $this->detectorName, $this->prefix));
        $persister->save($estimator);
        return $estimator->trained();
    }

    /**
     * @param $detector
     * @param $dataType
     * @param null $samples
     * @param string|false $prefix
     * @param false $output
     * @return mixed
     */
    public static function checkModel($detector, $dataType, $samples = null, $prefix = false, $output = false)
    {
        /** @var Persister $persister */
        $persister = new Filesystem(static::getModelLocation($dataType, $detector, $prefix));
        $estimator = $persister->load();
        if ($samples) {
            $dataSet = new Unlabeled($samples);
        } else {
            $samples = static::CHECK_DATA_PRESETS[$dataType];
            $dataSet = new Unlabeled($samples);
        }
        $scores = $estimator->score($dataSet);
        if ($output == 'cli') {
            static::printCli($scores, $samples, static::DATA_TYPES[$dataType]['titles']);
        }
        return $scores;
    }

    /**
     * @param $scores
     * @param $samples
     * @param $titles
     */
    public static function printCli($scores, $samples, $titles)
    {
        foreach ($titles as $item) {
            echo $item, "\t\t";
        }
        echo PHP_EOL;
        foreach ($scores as $idx => $score) {
            foreach ($samples[$idx] as $value) {
                echo $value, "\t\t";
            }
            echo round($score, 4), "\t\t", PHP_EOL;
        }
    }
}
