<?php

namespace Library\AnomalyDetection;

use ClickHouseDB\Client;
use Library\ConsoleTask\Helper;
use Phalcon\Mvc\User\Component;

class DataExtractor extends Component
{
    public $function;
    public $offset = 0;

    /**
     * DataExtractor constructor.
     *
     * @param $dataType
     */
    public function __construct($dataType)
    {
        $this->function = $this->getExtractorByDataType($dataType);
    }

    /**
     * Return part of data
     *
     * @param $limit
     * @return mixed
     */
    public function extractPart($limit)
    {
        $extractor = $this->function;
        $dataSet = $this->$extractor($limit, $this->offset);
        $this->offset += $limit;
        return $dataSet;
    }

    /**
     * Return all data
     *
     * @param integer|null $limit
     * @return array
     */
    public function extractAll($limit = null)
    {
        $limit = $limit ?? Core::SIZE_OF_DATA_PART;
        $dataSet = [];
        $extractor = $this->function;
        while (true) {
            $dataPart = $this->$extractor($limit, $this->offset);
            $dataSet = array_merge($dataSet, $dataPart);
            if (count($dataPart) < $limit) {
                break;
            } else {
                $this->offset += $limit;
            }
        }
        return $dataSet;
    }

    /**
     * Return connection to ClickHouse
     *
     * @return Client
     */
    public function getDb()
    {
        return $this->getDI()->getShared('ClickHouseDB2');
    }

    /**
     * Return name of function by data type
     *
     * @param string $dataType
     * @return string
     */
    public function getExtractorByDataType($dataType)
    {
        return 'get' . implode('', array_map('ucfirst', explode('-', $dataType))) . 'Data';
    }

    /**
     * Returns prepared dataset
     *
     * @param integer $limit
     * @param integer $offset
     * @return array
     */
    public function getGameIdBetsRtpWinByPlayerData($limit, $offset)
    {
        $rows = $this->getDb()->select(
            "SELECT game_id, count(*) AS count_bets,
                   round(coalesce(sum(return_eur)/sum(stake_eur),0) * 100, 4)  AS rtp,
                   round((sum(if(return_eur > 0, 1, 0)) /count(*)) * 100, 4) won
              FROM bets
             WHERE bet_status_id = 2
               AND is_demo = 0
             GROUP BY player_id_unique, game_id
             ORDER BY player_id_unique
             LIMIT :limit
            OFFSET :offset;",
            [
                'limit' => $limit,
                'offset' => $offset
            ]
        )->rows();
        return $this->prepareUnlabeledData($rows);
    }

    /**
     * Returns prepared dataset
     *
     * @param integer $limit
     * @param integer $offset
     * @return array
     */
    public function getGameIdBetsRtpWinByPlayerByHourData($limit, $offset)
    {
        $rows = $this->getDb()->select(
            "SELECT game_id, count(*) AS count_bets,
                   round(coalesce(sum(return_eur)/sum(stake_eur),0) * 100, 4)  AS rtp,
                   round((sum(if(return_eur > 0, 1, 0)) /count(*)) * 100, 4) won
              FROM bets
             WHERE bet_status_id = 2
               AND is_demo = 0
             GROUP BY player_id_unique, game_id, toStartOfHour(settle_date)
             ORDER BY player_id_unique
             LIMIT :limit
            OFFSET :offset;",
            [
                'limit' => $limit,
                'offset' => $offset
            ]
        )->rows();
        return $this->prepareUnlabeledData($rows);
    }

    /**
     * Returns prepared dataset
     *
     * @param integer $limit
     * @param integer $offset
     * @return array
     */
    public function getGameIdBetsWinByPlayerByHourData($limit, $offset)
    {
        $rows = $this->getDb()->select(
            "SELECT game_id, count(*) AS count_bets,
                   round((sum(if(return_eur > 0, 1, 0)) /count(*)) * 100, 4) won
              FROM bets
             WHERE bet_status_id = 2
               AND is_demo = 0
             GROUP BY player_id_unique, game_id, toStartOfHour(settle_date)
             ORDER BY player_id_unique
             LIMIT :limit
            OFFSET :offset;",
            [
                'limit' => $limit,
                'offset' => $offset
            ]
        )->rows();
        return $this->prepareUnlabeledData($rows);
    }

    /**
     * Returns prepared dataset
     *
     * @param integer $limit
     * @param integer $offset
     * @return array
     */
    public function getGameTypeBetsRtpWinByPlayerData($limit, $offset)
    {
        $rows = $this->getDb()->select(
            "SELECT game_type_id, count(*) AS count_bets,
                   round(coalesce(sum(return_eur)/sum(stake_eur),0) * 100, 4)  AS rtp,
                   round((sum(if(return_eur > 0, 1, 0)) /count(*)) * 100, 4) won
              FROM bets
             WHERE bet_status_id = 2
               AND is_demo = 0
             GROUP BY player_id_unique, game_type_id
             ORDER BY player_id_unique
             LIMIT :limit
            OFFSET :offset;",
            [
                'limit' => $limit,
                'offset' => $offset
            ]
        )->rows();
        return $this->prepareUnlabeledData($rows);
    }

    /**
     * Returns prepared dataset
     *
     * @param integer $limit
     * @param integer $offset
     * @return array
     */
    public function getBetsRtpByPlayerData($limit, $offset)
    {
        $rows = $this->getDb()->select(
            "SELECT count(*) AS count_bets,
                   round(coalesce(sum(return_eur)/sum(stake_eur),0) * 100, 4)  AS rtp
              FROM bets
             WHERE bet_status_id = 2
               AND is_demo = 0
             GROUP BY player_id_unique
             ORDER BY player_id_unique
             LIMIT :limit
            OFFSET :offset;",
            [
                'limit' => $limit,
                'offset' => $offset
            ]
        )->rows();
        return $this->prepareUnlabeledData($rows);
    }

    /**
     * Returns prepared dataset
     *
     * @param integer $limit
     * @param integer $offset
     * @return array
     */
    public function getPlayerData()
    {
        $rows = $this->getDb()->select(
            "select game_id, count(*) as count_bets, round(coalesce(sum(return_eur)/sum(stake_eur),0) * 100, 4)  as rtp,  round((sum(if(return_eur > 0, 1, 0)) /count(*)) * 100, 4) as won
                from bets
                where bet_status_id = 2
                AND is_demo = 0
                AND player_id_unique = 3023430016
                group by player_id_unique, game_id, toStartOfHour(settle_date)",
            [
//                'limit' => $limit,
//                'offset' => $offset
            ]
        )->rows();
        return $this->prepareUnlabeledData($rows);
    }

    /**
     * Remove keys and prepare data to unlabeled dataset
     *
     * @param array $dataSet
     * @return array
     */
    public function prepareUnlabeledData($dataSet)
    {
        array_walk(
            $dataSet,
            function (&$row) {
                $row = array_values(array_map('floatval', $row));
            }
        );
        return $dataSet;
    }
}
